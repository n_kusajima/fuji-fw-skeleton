<?php


namespace App\Constants;


/**
 * 都道府県名const
 * Class Prefecture
 * @package App\Constants
 */
final class Prefecture extends AbstractConstants
{
    const ENUM = [
        'HK' => '北海道',
        'AO' => '青森県',
        'IT' => '岩手県',
        'MG' => '宮城県',
        'AK' => '秋田県',
        'YG' => '山形県',
        'FS' => '福島県',
        'IB' => '茨城県',
        'TC' => '栃木県',
        'GU' => '群馬県',
        'ST' => '埼玉県',
        'CB' => '千葉県',
        'TY' => '東京都',
        'KN' => '神奈川県',
        'NI' => '新潟県',
        'TM' => '富山県',
        'IS' => '石川県',
        'FI' => '福井県',
        'YN' => '山梨県',
        'NA' => '長野県',
        'GI' => '岐阜県',
        'SZ' => '静岡県',
        'KY' => '京都府',
        'OS' => '大阪府',
        'HG' => '兵庫県',
        'NR' => '奈良県',
        'WA' => '和歌山県',
        'TT' => '鳥取県',
        'SM' => '島根県',
        'OY' => '岡山県',
        'HS' => '広島県',
        'YA' => '山口県',
        'TK' => '徳島県',
        'KA' => '香川県',
        'EH' => '愛媛県',
        'KO' => '高知県',
        'FO' => '福岡県',
        'SG' => '佐賀県',
        'NS' => '長崎県',
        'KU' => '熊本県',
        'OI' => '大分県',
        'MZ' => '宮崎県',
        'KG' => '鹿児島県',
        'OK' => '沖縄県'
    ];

    public static function __callStatic($name, $arguments)
    {
        return new self($name);
    }
}