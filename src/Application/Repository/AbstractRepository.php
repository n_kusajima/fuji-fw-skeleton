<?php


namespace App\Application\Repository;


use Psr\Container\ContainerInterface;

abstract class AbstractRepository implements RepositoryInterface
{
    /** @var \PDO */
    protected $db;

    public function __construct(ContainerInterface $c)
    {
        $this->db = $c->get('db');
    }

    public function beginTransaction()
    {
        $this->db->beginTransaction();
    }

    public function commit()
    {
        $this->db->commit();
    }

    public function rollback()
    {
        $this->db->rollBack();
    }

    public function transaction(callable $callback) :void
    {
        $this->beginTransaction();
        try {
            $callback();
            $this->commit();
        } catch (\PDOException $e) {
            $this->rollback();
            throw $e;
        }
    }

}