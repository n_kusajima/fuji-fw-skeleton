<?php

// Application middleware

// e.g: $app->add(new \Slim\Csrf\Guard);

$app->add(new Slim\Middleware\Session([
    'name' => getenv('APP_NAME'),
    'autorefresh' => true
]));