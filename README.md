# Fujiball FW with Slim Skeleton
This is a simple skeleton project for a fujiball framework with Slim3.  
Included monolog, slim-session, Smarty and slim-flash. 

## requirements
* PHP >= 5.5.0

## How to install
```bash
git clone https://gitlab.com/n_kusajima/fuji-fw-skeleton.git
cd fuji-fw-skeleton
composer install
```

## How to run
```bash
cd ${your_project}
cp .env.example .env # and modify .env according to your enviroment.
```

* use docker
    ```bash
    docker-copmpose up -d 
    ```
* use Built-in server
    ```bash
    composer start 
    ```

Now see in your browser `localhost:8080` !