<?php

use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Monolog\Processor\UidProcessor;
use Psr\Container\ContainerInterface;
use Slim\Views\Smarty;


$container = $app->getContainer();

// logger
$container['logger'] = function (ContainerInterface $c) {
    $settings = $c->get('settings');

    $loggerSettings = $settings['logger'];
    $logger = new Logger($loggerSettings['name']);

    $processor = new UidProcessor();
    $logger->pushProcessor($processor);

    $handler = new StreamHandler($loggerSettings['path'], $loggerSettings['level']);
    $logger->pushHandler($handler);

    return $logger;
};

// smarty
$container['view'] = function (ContainerInterface $c) {
    $settings = $c->get('settings');

    $view = new Smarty(
        $settings['view']['template_path'], [
            'compileDir' => $settings['view']['compiled_path'],
            'cacheDir' => $settings['view']['cache_path']
        ]);

    $smartyPlugins = new \Slim\Views\SmartyPlugins($c['router'], $c['request']->getUri());
    $view->registerPlugin('function', 'path_for', [$smartyPlugins, 'pathFor']);
    $view->registerPlugin('function', 'base_url', [$smartyPlugins, 'baseUrl']);

    return $view;
};

// db
$container['db'] = function(ContainerInterface $c) {
    $settings = $c->get('settings');
    $dsn = $settings['db']['connection'] . ':'
        . 'host=' . $settings['db']['host'] .';'
        . 'dbname=' . $settings['db']['dbname'] . ';'
        . 'port=' . $settings['db']['port']. ';';
    return new PDO(
        $dsn,
        $settings['db']['user'],
        $settings['db']['password'],
        [
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
        ]
    );
};

// flash
$container['flash'] = function () {
    return new \Slim\Flash\Messages();
};

// session helper
$container['session'] = function () {
    return new SlimSession\Helper();
};