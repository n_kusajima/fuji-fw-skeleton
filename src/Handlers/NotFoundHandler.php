<?php


namespace App\Handlers;


use Psr\Container\ContainerInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Slim\Http\Response;
use Slim\Views\Smarty;

class NotFoundHandler
{
    /** @var Smarty */
    protected $view;

    public function __construct(ContainerInterface $container)
    {
        $this->view = $container->get('view');
    }

    public function __invoke(RequestInterface $request, ResponseInterface $response)
    {
        $response = new Response(404);
        return $this->view->render($response, 'errors/404.tpl');
    }
}