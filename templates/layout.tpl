<!DOCTYPE html>
<html lang="ja">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0">
    <title>Fujiball sample App</title>
    <link rel="stylesheet" href="/css/material.min.css">
    <script src="/js/material.js"></script>
    <link rel="stylesheet" href="//fonts.googleapis.com/icon?family=Material+Icons">

    {block name=css}{/block}
</head>
<body>
    <div class="mdl-layout mdl-js-layout mdl-layout--fixed-drawer">
        <div class="mdl-layout__drawer">
            <span class="mdl-layout-title">Sample App</span>
            <nav class="mdl-navigation">
                <a class="mdl-navigation__link" href="">User List</a>
            </nav>
        </div>

        <main class="mdl-layout__content">
            <div class="page-content">{block name=content}{/block}</div>
        </main>
    </div>
{block name=js}{/block}
</body>
<footer></footer>
</html>
