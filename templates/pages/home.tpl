{extends file='../layout.tpl'}

{block css}
    <style type="text/css">
    .demo-padding {
        width: 50%;
        display: table-cell;
        vertical-align: middle;
        margin: 0;
    }
    </style>
{/block}

{block name=content}
    <div class="demo-padding"></div>
    <table class="mdl-data-table mdl-js-data-table mdl-shadow--2dp">
        <thead>
        <tr>
            <th>ID</th>
            <th>name</th>
            <th>gender</th>
            <th>address</th>
            <th>hobby</th>
        </tr>
        </thead>
        <tbody>
            <tr>
                <td>
                    <button class="mdl-button mdl-js-button">1</button>
                </td>
                <td>Shiobe</td>
                <td>Male</td>
                <td>Nerima, Tokyo</td>
                <td>movie</td>
            </tr>
        </tbody>
    </table>
    <div class="demo-padding"></div>
{/block}
