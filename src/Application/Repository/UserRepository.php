<?php


namespace App\Application\Repository;

class UserRepository extends AbstractRepository
{
    public function fetchAll()
    {
        $sql = 'SELECT * FROM user WHERE deleted_at IS NULL';

        /** @var \PDOStatement $stmt */
        $stmt = $this->db->prepare($sql);
        $stmt->execute();

        return $stmt->fetchAll();
    }

    public function findById($id)
    {
        $sql = 'SELECT * FROM user WHERE delete_at IS NULL AND id = :id';

        /** @var \PDOStatement $stmt */
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':id', $id);
        $stmt->execute();

        return $stmt->fetch();
    }

    public function search(array $params, int $limit = null, int $offset = null)
    {
        // TODO: Implement search() method.
    }

    public function insert(array $params)
    {
        // TODO: Implement insert() method.
    }

    public function update(array $params)
    {
        // TODO: Implement update() method.
    }
}