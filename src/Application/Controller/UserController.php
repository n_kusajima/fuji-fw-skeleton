<?php


namespace App\Application\Controller;


use App\Application\Repository\UserRepository;
use App\Constants\Gender;
use App\Helpers\SmartyHelper;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

class UserController extends BaseController
{
    private const PAGE_LIMIT = 10;
    protected $repo;

    public function __construct(ContainerInterface $container)
    {
        parent::__construct($container);
        $this->repo = new UserRepository($container);
    }

    public function index(Request $request, Response $response, array $args)
    {
        $offset = isset($args['page']) ? $args['page'] - 1 : 0;

        $data = $this->repo->paging(self::PAGE_LIMIT, $offset, $args);

        $this->view->render($response, 'pages/user_list.tpl', [
            'user_list' => $data,
            'search_params' => $args
        ]);
    }

    public function new(Request $request, Response $response)
    {
        // confirm or insert
        if ($request->isPost()) {
            $inputs = $request->getParsedBody();
            // insert
            $this->repo->insert($inputs);
        }
        $this->view->render($response, 'pages/user_form.tpl');
    }

}