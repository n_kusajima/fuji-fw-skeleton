<?php


namespace App\Application\Repository;


interface RepositoryInterface
{
    public function fetchAll();

    public function findById($id);

    public function search(array $params, $limit = null, $offset = null);

    public function insert(array $params);

    public function update(array $params);

    public function beginTransaction();

    public function commit();

    public function rollback();

    public function transaction(callable $call);
}