<?php


namespace App\Constants;


/**
 * 性別const
 * Class Gender
 * @package App\Constants
 */
final class Gender extends AbstractConstants
{
    const ENUM = [
        'M' => '男性',
        'F' => '女性'
    ];

    /**
     * @param $name
     * @param $arguments
     * @return Gender
     */
    public static function __callStatic($name, $arguments)
    {
        return new self($name);
    }
}