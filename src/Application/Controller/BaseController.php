<?php
namespace App\Application\Controller;

use Monolog\Logger;
use Psr\Container\ContainerInterface;
use Slim\Views\Smarty;
use SlimSession\Helper;

class BaseController
{
    /** @var Logger  */
    protected $logger;

    /** @var \PDO  */
    protected $db;

    /** @var Smarty */
    protected $view;

    /** @var Helper */
    protected $session;

    /**
     * BaseController constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->logger = $container->get('logger');
        $this->db = $container->get('db');
        $this->view = $container->get('view');
        $this->session = $container->get('session');
    }

}