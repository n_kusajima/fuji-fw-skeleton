<?php
namespace App\Application\Controller;

use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

class HomeController extends BaseController
{
    public function index(Request $request, Response $response, array $args)
    {
        return $this->view->render($response,'pages/home.tpl');
    }
}