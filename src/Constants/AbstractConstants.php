<?php


namespace App\Constants;


use http\Exception\InvalidArgumentException;

/**
 * 抽象化constクラス
 * Class AbstractConstants
 * @package App\Constants
 */
abstract class AbstractConstants
{
    const ENUM = [];

    private $key;
    private $value;

    public function __construct($key)
    {
        if(!static::isValidKey($key)) {
            throw  new InvalidArgumentException;
        }
        $this->key = $key;
        $this->value = self::ENUM[$key];
    }

    abstract public static function __callStatic($name, $arguments);

    public function getKey()
    {
        return $this->key;
    }

    public static function isValidKey($key)
    {
        return array_key_exists($key, self::ENUM);
    }

    public function __set($name, $value)
    {
        throw new \BadMethodCallException('All setter is forbidden');
    }

    public function __toString()
    {
        return $this->value;
    }

    /**
     * ENUMのkeyをinputタグのvalueに持ち、labelにENUMのvalueを持つcheck boxを生成
     * @param string $tag_name htmlタグname属性
     * @param array $classes 付与するクラスリスト
     * @param array $checked checked属性をtrueにしたいcheckboxのvalue値(ENUMのkey)
     * @return string
     */
    public static function createCheckBox(string $tag_name, array $classes = [], array $checked = []) : string
    {
        return self::createCheckOrRadio('checkbox', $tag_name, $classes, $checked);
    }

    /**
     * ENUMのkeyをinputタグのvalueに持ち、labelにENUMのvalueを持つradio boxを生成
     * @param string $tag_name htmlタグname属性
     * @param array $classes 付与するクラスリスト
     * @param string $checked checked属性をtrueにしたいradioボタンのvalue値(ENUMのkey)
     * @return string
     */
    public static function createRadioBox(string $tag_name, array $classes = [], string $checked = null) : string
    {
        $checked = is_null($checked) || empty($checked) ? [] : [$checked];
        return self::createCheckOrRadio('radio', $tag_name, $classes, $checked);
    }

    /**
     * @param string $type
     * @param string $tag_name
     * @param array $classes
     * @return string
     */
    private static function createCheckOrRadio(string $type, string $tag_name, array $classes, $checked = []) : string
    {
        if (!in_array($type, ['radio', 'checkbox'], true)) {
            throw new InvalidArgumentException;
        }

        $ret = '';
        $class = implode(' ', $classes);
        foreach (self::ENUM as $key => $value) {
            $is_checked = in_array($key, $checked);
            $ret.= "<input type='${type}' name='${tag_name}' class='${class}' id='${tag_name}-${key}'"
                    ." value='${key}' checked='${is_checked}'>";
            $ret.= "<label for='${tag_name}-${key}'>${value}</label>";
        }

        return $ret;
    }

    /**
     * ENUM内のkey, valueを持つセレクトボックス形式で出力する
     * @param string $tag_name
     * @param array $classes
     * @return string
     */
    public static function createSelectBox(string $tag_name, array $classes = []) : string
    {
        $class = implode(' ', $classes);

        $ret = "<select name='${tag_name}' class='${class}'>";
        foreach (self::ENUM as $key => $value) {
            $ret.="<option value='$key'>${value}</option>";
        }
        $ret.= "</select>";

        return $ret;
    }
}