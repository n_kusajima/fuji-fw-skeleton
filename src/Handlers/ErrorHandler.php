<?php


namespace App\Handlers;


use Psr\Container\ContainerInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Slim\Http\Response;
use Slim\Views\Smarty;

class ErrorHandler
{
    /** @var Smarty */
    protected $view;

    public function __construct(ContainerInterface $container)
    {
        $this->view = $container->get('view');
    }

    public function __invoke(RequestInterface $request, ResponseInterface $response, $e)
    {
        $response = new Response(500);
        if (getenv('DISPLAY_ERROR')) {
            return $response->withHeader('Content-Type', 'text/html')
                ->write('Something went wrong!<br>' . $e->getMessage() );
        } else {
            return $this->view->render($response, 'errors/50x.tpl');
        }
    }

}