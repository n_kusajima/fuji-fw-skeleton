<?php

use App\Handlers\ErrorHandler;
use App\Handlers\NotFoundHandler;
use Dotenv\Dotenv;
use Psr\Container\ContainerInterface;

if (PHP_SAPI == 'cli-server') {
    // To help the built-in PHP dev server, check if the request was actually for
    // something which should probably be served as a static file
    $url  = parse_url($_SERVER['REQUEST_URI']);
    $file = __DIR__ . $url['path'];
    if (is_file($file)) {
        return false;
    }
}

// project root
$root_dir = __DIR__ . '/../';

require $root_dir . 'vendor/autoload.php';

session_start();

// load env file.
$env = Dotenv::create(__DIR__ . '/../');
$env->load();

$settings = require $root_dir . 'configs/settings.php';

$app = new \Slim\App($settings);

require $root_dir . 'configs/dependencies.php';

require $root_dir . 'configs/middleware.php';

require $root_dir . 'configs/routes.php';

$c = $app->getContainer();

// register error handler
$c['errorHandler'] = function (ContainerInterface $c) {
    return new ErrorHandler($c);
};

$c['notFoundHandler'] = function (ContainerInterface $c) {
    return new NotFoundHandler($c);
};

$app->run();